# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import datetime
from collections import OrderedDict

# Enter your subreddit here
SUB = "yoursubreddit"
# You can change your user agent here but that's not necessary
USER_AGENT = "Sidebar RSS feed bot by /u/efflicto written for /r/{}".format(SUB)

# Tags in your sidebar. The feeds will displayed between these tags. Add them to your sidebar
START_TAG = "[](#Start)"
END_TAG = "[](#End)"

# Add your RSS feeds here like the example down below
RSS_FEEDS = OrderedDict({
    "Example 1": "http://www.example.com/storage/rss/rss/first.xml",
    "Example 2": "http://www.example.com/storage/rss/rss/second.xml",
})

# You can add a bottom text like this or just leave it blank
BOTTOM_TEXT = "\n\nLast change: {:%d.%m.%y %H:%M:%S}".format(datetime.datetime.now())

# Maximum messages per feed
MAX_ENTRIES = 5

# If there are no messages in the RSS feed this text will be displayed under the header
RSS_EMPTY_WORD = "No messages"

# Your Reddit client ID
CLIENT_ID = ""
# Your Reddit client secret
CLIENT_SECRET = ""
# Your username or your bots username
USERNAME = ""
# Your password or your bots password
PASSWORD = ""