# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import feedparser
import logging
from datetime import datetime

logger = logging.getLogger("logger")


class Feed:

    def __init__(self, title, link, text, date):
        self.title = title
        self.link = link
        self.text = text
        self.date = date


def get_feeds(url):
    f = feedparser.parse(url)
    feeds = []
    for feed in f['entries']:
        try:
            date_string = feed['published'].split(',')[1].lstrip().split('+')[0].rstrip()
            date = datetime.strptime(date_string, "%d %b %Y %H:%M:%S")
            text = str(feed['summary_detail']['value'])
            text = text.replace('&uuml;', 'ü')
            text = text.replace('&auml;', 'ä')
            text = text.replace('&ouml;', 'ö')
            text = text.replace('&Uuml;', 'Ü')
            text = text.replace('&Auml;', 'Ä')
            text = text.replace('&Ouml;', 'Ö')
            text = text.replace('&szlig;', 'ß')
            feeds.append(
                Feed(
                    title=feed['title_detail']['value'],
                    link=feed['link'],
                    text=text,
                    date=date
                     )
            )
        except Exception as e:
            logger.exception(e)

    return feeds