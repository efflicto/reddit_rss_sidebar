# -*- coding: utf-8 -*-
#!/usr/bin/env python3
try:
    import config
except:
    raise FileNotFoundError("Config file does not exist. Please read the README.")
import errno
import logging
import praw
import os
import sys
from collections import OrderedDict
from feed import get_feeds
from logging.handlers import RotatingFileHandler


logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
try:
    os.makedirs("log")
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
handler = RotatingFileHandler('log/log.log',
                              maxBytes=1000000,
                              backupCount=10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [MAIN] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)


def compile_feeds():
    feeds = OrderedDict()
    for f, url in config.RSS_FEEDS.items():
        feeds[f] = get_feeds(url=url)

    return feeds


def main():
    try:
        logger.info("Reddit RSS Sidebar Bot started.")
        logger.info("Compile feeds.")
        feeds = compile_feeds()
        logger.info("Feeds compiled. Feeds: {}".format(len(feeds.keys())))
        logger.info("Getting Reddit session.")
        try:
            reddit_session = praw.Reddit(client_id=config.CLIENT_ID,
                                         client_secret=config.CLIENT_SECRET,
                                         username=config.USERNAME,
                                         password=config.PASSWORD,
                                         user_agent=config.USER_AGENT
                                         )
        except Exception as e:
            logger.exception(e)
        logger.info("Got the Reddit session. Trying to obtain the subreddit.")
        sub = reddit_session.subreddit(config.SUB)
        logger.info("Got the subreddit. Trying to obtain the sidebar.")
        sidebar_text = sub.description
        logger.info("Got the sidebar. Compiling the new sidebar text.")
        sidebar_top = sidebar_text.split(config.START_TAG)[0]
        sidebar_bottom = sidebar_text.split(config.END_TAG)[1]

        rss_feeds = ""

        for feed, fe in feeds.items():
            rss_feeds += "\n\n***\n\n####**{}:**\n\n".format(feed)
            if len(fe) == 0:
                rss_feeds += "\n\n{}".format(config.RSS_EMPTY_WORD)
            else:
                for x in fe[:config.MAX_ENTRIES]:
                    date = "{:%d.%m.%y %H:%M}".format(x.date)
                    rss_feeds += "\n\n#####{}: [{}]({})".format(date, x.title, x.link)
        sidebar_new = "{}{}{}{}{}{}".format(
            sidebar_top,
            config.START_TAG,
            rss_feeds,
            config.BOTTOM_TEXT,
            config.END_TAG,
            sidebar_bottom)
        logger.info("New sidebar text compiled. Replacing the old one...")
        sub_mod = sub.mod

        sub_mod.update(description=sidebar_new)
        logger.info("Done.")
    except KeyboardInterrupt as e:
        logger.exception(e)
        sys.exit(0)
    except Exception as e:
        logger.exception(e)

if __name__ == '__main__':
    main()

